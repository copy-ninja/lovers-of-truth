const routes = [
  {
    path: "/",
    component: () => import("layouts/RadioLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/photos", component: () => import("pages/Photos.vue") },
      { path: "/youtube", component: () => import("pages/Youtube.vue") },
      { path: "/about", component: () => import("pages/About.vue") },
            { path: "/extracts", component: () => import("pages/Extracts.vue") },
            { path: "/extracts/:date", component: () => import("pages/_Extract.vue") },

      // { path: "/privacy", component: () => import("pages/Privacy.vue") }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
